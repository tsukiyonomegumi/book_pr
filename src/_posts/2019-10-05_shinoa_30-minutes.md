---
title: 紫のあ「30 MINUTES」
lang: ja-JP
layout: Post
published: 2019-10-05
tags:
 - COMITIA129
 - 百合
 - 同人
category: 同人
meta:
  - property: og:title
    content: 紫のあ「30 MINUTES」の感想
  - property: og:image
    content: https://tsukiyonomegumi.gitlab.io/book_pr/img/_posts/shinoa_30-minutes_sum.jpg
  - name: description
    content: 紫のあ「30 MINUTES」
  - property: og:description
    content: 紫のあ「30 MINUTES」
  - property: og:type
    content: article
---

# 概要

- タイトル: 30 MINUTES.
- 著者: 紫のあ [Twitter](https://twitter.com/sinoa_ster)
- 形式等
  - B5 26ページ 500円
  - サンプル [pixiv](https://www.pixiv.net/artworks/76403340)
  - 通販/委託 [BOOTH](https://sinoa-ster.booth.pm/items/1554524)

![表紙](/book_pr/img/_posts/shinoa_30-minutes.jpg)

## あらすじ

女の子2人組は、以前付き合っていた人に終電で返された話から始まる。  
終電10分前となったが、愚痴を言っていた子が「どうする？」と帽子を被った子に聞くが・・・
お互いの**駆け引き**をたのしみながら読むことのできる百合なラブストーリー。

## 感想

お互いに素直になれない子同士、駆け引きをしながらどうやって泊まろうか駆け引きをする様子がたまらない。 
帽子を被っている子は素直に言い出せない引っ込み思案な性格だが、相手の子も「以前付き合っていた子に終電で返されたの」「終電まで10分くらいで、急がなくても間に合うけど**どうする？**」と聞いてくるなど、この子もこの子でなかなかに素直じゃないが、それによってさらに話を引き立ててくれる。お互いめんどくさい。でもそれが非常に良い。  
そして「タイトルいは30分ってあるのに、これじゃあ10分とか15分じゃないの？」と思われると思うが、それは最後まで読めば納得。


個人的にこの作品の技法としていいなと感じたのは、キャラの会話の間合いを句読点ではなく、吹き出しで区切っている。そのことによって帽子を被っている子の「言い出したいけど素直に言い出せない」特有の間合いを非常にうまく表現できており、終電10分前の夜の空気を更に引き立ててくれ非常に良い。あのもじもじしているときになかなか言い出せない、独特の間合いを感じることをできて「あ〜〜〜あまずっぺ〜〜〜〜！！」となりながら読んでしまった。  

## 補足

筆者の本はCOMITIA 128で発行された[バイ・マイ・サイド](https://sinoa-ster.booth.pm/items/1385303)にて、その独特の空気を感じる世界観・設定に魅了されて今回も購入させて頂いた。  
前作のバイ・マイ・サイドはpixivにて公開されているため、気になる方はぜひ。  
 [花が生えた人の話 - 紫のあのイラスト - pixiv](https://www.pixiv.net/artworks/73456236)

ちなみに以下はおまけとしてついてきたペーパー。お互い手を合わせていたり、あっかんべーしていたりかわいい。部屋に飾ろう。  
ちなみに概要にあるBOOTHからペーパーのpdfはダウンロードできます（この記事書いてるときに気づいた）
![おまけのペーパー](/book_pr/img/_posts/shinoa_30-minutes_omake.jpg)


