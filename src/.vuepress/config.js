module.exports = {
  title: 'BOOK P&R',
  description: '読んだ本の感想を書いたり紹介するところ',
  dest: 'public',
  base: '/book_pr/',
  theme: 'meteorlxy',
  themeConfig: {
    lang: Object.assign(require('vuepress-theme-meteorlxy/lib/langs/ja-JP'), {
      home: 'BOOK P&R',
    }),
    personalInfo: {
      nickname: 'tsukiyonomegumi',
      description: '本を読むおたく',
      avatar: '/book_pr/img/avatar.png',
      email: 'tsukiyonomegumi@gmail.com',
      location: 'Japan',
      sns: {
        gitlab: {
          account: 'Tsukiyono Megumi',
          link: 'https://gitlab.com/tsukiyonomegumi/',
        },
        twitter: {
          account: 'tsukiyonomegumi',
          link: 'https://twitter.com/tsukiyonomegumi',
        },
      },
    },
    nav: [
      { text: 'Home', link: '/', exact: true },
      { text: 'About', link: '/about', exact: true },
      { text: 'Posts', link: '/posts/', exact: false },
    ],
  },
  plugins: {
    'sitemap': {
      hostname: 'https://tsukiyonomegumi.gitlab.io/book_pr/'
    },
  },
  head: [
    ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Kosugi+Maru&display=swap&subset=japanese' }],
    ['link', { href: 'https://use.fontawesome.com/releases/v5.11.1/css/all.css', rel: 'stylesheet' }],
    ['link', { href: '/css/style.css', rel: 'stylesheet'}],
    ['meta', { property: 'og:site_name', content: 'BOOK P&R' }],
    ['meta', { name: 'twitter:card', content: 'summary_large_image' }],
    ['meta', { name: 'twitter:site', content: '@tsukiyonomegumi' }],
    ['meta', { name: 'twitter:creator', content: '@tsukiyonomegumi' }],
  ],
  header: {
    background: {
      useGeo: true,
    },
    showTitle: true,
    lastUpdated: true,
    pagination: {
      perPage: 5,
    },
    defaultPages: {
      home: true,
      posts: true,
    },
  },
}
