---
title: About
lang: ja-JP
layout: Post
published: 2019-09-12
meta:
  - property: og:title
    content: About
  - property: og:image
    content: https://tsukiyonomegumi.gitlab.io/book_pr/img/og_dummy.img
  - name: description
    content: About
  - property: og:description
    content: About
  - name: keywords
    content: About
  - property: og:type
    content: article
---

# About

このサイトは筆者が即売会などを通して入手した同人誌や、購入した商業誌の感想をつらつらと書くところです。  
twitterなどのミニブログでは制限なので書ききれないことなどを書いていきたいですね。  

以前はどこぞで感想をかくことなんてありませんでしたが、以前から消滅や筆を折った作者さんを何度も目にしてきてしまい、壁打ちでも感想を書けたらなと思ったのがこのサイトを作ったきっかけです。  
あまり文章はうまくないですが、気持ちが伝わればいいですね・・・・


## 筆者について

趣味で同人誌を15〜16歳くらいで読み始めた。  
以前は二次創作が中心だったが、いつの間にかイラストや一次創作漫画にどっぷり。  
即売会ではかなり買ったりしていたが最近は抑えめ。  
作品買いというよりは作者買いするタイプ。  

どっかの即売会のカタログに同人誌の感想を載せてもらったりたまにする。

[Twitter:@tsukiyonomegumi](https://twitter.com/tsukiyonomegumi)

## このサイトの技術的なこと

- gitlab pageにて公開しています。記事書いたらgitlab ciで自動コンパイルしてサイトの更新までしてくれるので楽です。  
  - ソースコード [Tsukiyono Megumi/BOOK_PR](https://gitlab.com/tsukiyonomegumi/book_pr)
  - gitlab pagesの容量がつらくなったらNetlifyに移行かなぁと言う感じ。
- サイトエンジンは[VuePress](https://vuepress.vuejs.org/)を使用しています。
  - まだリリースされて間もないのでHexoやHugoなどよりかは機能がまだ控えめです。
    - 初期の設定がHexoなどよりかは手間かなと感じました。
    - OGPの組み込みがとても面倒だった・・・
